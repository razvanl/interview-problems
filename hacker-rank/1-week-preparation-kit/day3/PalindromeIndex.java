import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result {

    /*
     * Complete the 'palindromeIndex' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     */

    public static int palindromeIndex(String s) {
        return palindromeIndex(s, 0, s.length() - 1, -1);
    }
    
    public static int palindromeIndex(String s, int start, int end, int index) {        
        if (start >= end) {
            return index;
        }
        
        if (s.charAt(start) == s.charAt(end)) {
            return palindromeIndex(s, start + 1, end - 1, index);
        } else {
            if (index != -1) {
                return -1;
            }
            
            int skipLeft = palindromeIndex(s, start + 1, end, start);
            int skipRight = palindromeIndex(s, start, end - 1, end);

            return skipLeft == -1 ? skipRight : skipLeft;
        }
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, q).forEach(qItr -> {
            try {
                String s = bufferedReader.readLine();

                int result = Result.palindromeIndex(s);

                bufferedWriter.write(String.valueOf(result));
                bufferedWriter.newLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        bufferedReader.close();
        bufferedWriter.close();
    }
}
