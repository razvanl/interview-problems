// It is New Year's Day and people are in line for the Wonderland rollercoaster ride. 
// Each person wears a sticker indicating their initial position in the queue from  to . 
// Any person can bribe the person directly in front of them to swap positions, but they still wear their original sticker. 
// One person can bribe at most two others.
// Determine the minimum number of bribes that took place to get to a given queue order. 
// Print the number of bribes, or, if anyone has bribed more than two people, print Too chaotic.

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'minimumBribes' function below.
     *
     * The function accepts INTEGER_ARRAY q as parameter.
     */

    public static void minimumBribes(List<Integer> q) {
        String output = isTooChaostic(q) ? "Too chaotic" : "" + calculateBribes(q);
        System.out.println(output);
    }
    
    private static boolean isTooChaostic(List<Integer> q) {
        for (int i = 0; i < q.size() - 1; i++) {
            if (q.get(i) - i - 1 > 2 || q.get(i + 1) - i - 2 > 2) {
                return true;
            }
        }
        return false;
    }
    
    private static int calculateBribes(List<Integer> q) {
        int bribes = 0;

        for (int i = 0; i < q.size(); i++) {
            boolean swapped = false;
            
            for (int j = 0; j < q.size() - i - 1; j++) {
                if (q.get(j) > q.get(j + 1)) {
                    int aux = q.get(j);
                    q.set(j, q.get(j + 1));
                    q.set(j + 1, aux);
                    swapped = true;
                    bribes++;     
                }
            }
            
            if (!swapped) {
                break;
            }
        }
        
        return bribes;
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int t = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, t).forEach(tItr -> {
            try {
                int n = Integer.parseInt(bufferedReader.readLine().trim());

                List<Integer> q = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                    .map(Integer::parseInt)
                    .collect(toList());

                Result.minimumBribes(q);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        bufferedReader.close();
    }
}
