// Given a square grid of characters in the range ascii[a-z], rearrange elements of each row alphabetically,
//  ascending. Determine if the columns are also in ascending alphabetical order, top to bottom. 
// Return YES if they are or NO if they are not.

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'gridChallenge' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING_ARRAY grid as parameter.
     */

    public static String gridChallenge(List<String> grid) {
        List<String> sortedGrid = grid.stream().map(Result::sortChars).collect(Collectors.toList());        
        return areColumnsSorted(sortedGrid);
    }
    
    private static String sortChars(String s) {
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        String sortedString = "";
        
        for (char c : chars) {
            sortedString += c;
        }
        
        return sortedString;
    }
    
    private static String areColumnsSorted(List<String> grid) {
        int size = grid.get(0).length();
        for (int i = 0; i < size; i++) {
            if (!isColumnSorted(grid, i)) {
                return "NO";
            }
        }
        return "YES"; 
    }
    
    private static boolean isColumnSorted(List<String> grid, int index) {
        char lastChar = 'a';
        
        System.out.println("Index = " + index);
        System.out.println("Grid length = " + grid.size());
        
        for (String el : grid) {
            char searchedChar = el.charAt(index);
            if (lastChar <= searchedChar) {
                lastChar = searchedChar;
            } else {
                return false;
            }
        }
        
        return true;
    }
    
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int t = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, t).forEach(tItr -> {
            try {
                int n = Integer.parseInt(bufferedReader.readLine().trim());

                List<String> grid = IntStream.range(0, n).mapToObj(i -> {
                    try {
                        return bufferedReader.readLine();
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                })
                    .collect(toList());

                String result = Result.gridChallenge(grid);

                bufferedWriter.write(result);
                bufferedWriter.newLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        bufferedReader.close();
        bufferedWriter.close();
    }
}
