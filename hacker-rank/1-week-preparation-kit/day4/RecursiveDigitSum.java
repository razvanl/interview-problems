// We define super digit of an integer  using the following rules:
// Given an integer, we need to find the super digit of the integer.
// If  has only  digit, then its super digit is .
// Otherwise, the super digit of  is equal to the super digit of the sum of the digits of .

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'superDigit' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. STRING n
     *  2. INTEGER k
     */

    public static int superDigit(String n, int k) {
        long s = k * superDigit(n);
        
        while (s > 10) {
            s = superDigit(s);
        }
        return (int) s;
    }
    
    private static long superDigit(String n) {
        long sum = 0;
        for (char c : n.toCharArray()) {
            sum += Integer.valueOf(String.valueOf(c));
        }
        return sum;
    }
    
    private static long superDigit(long n) {
        if (n < 10) {
            return n;
        } else {
            long sum = 0;
            
            while (n > 10) {
                sum += n % 10;
                n /= 10;
            }
            sum += n;
            return superDigit(sum);
        }
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        String n = firstMultipleInput[0];

        int k = Integer.parseInt(firstMultipleInput[1]);

        int result = Result.superDigit(n, k);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
