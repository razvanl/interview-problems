import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result {

    /*
     * Complete the 'truckTour' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY petrolpumps as parameter.
     */

    public static int truckTour(List<List<Integer>> petrolPumps) {
        for (int i = 0; i < petrolPumps.size(); i++) {
            if (destinationReached(i, (i + petrolPumps.size() - 1) % petrolPumps.size(), 0, petrolPumps)) {
                return i;
            }
        }
        return -1;
    }
    
    private static boolean destinationReached(int start, int end, int fuel, List<List<Integer>> petrolPumps) {
        fuel += petrolPumps.get(start).get(0);
        fuel -= petrolPumps.get(start).get(1);
        
        if (start == end) {
            return fuel >= 0;
        }

        if (fuel >= 0) {
            return destinationReached((start + 1) % petrolPumps.size(), end, fuel, petrolPumps);
        } else {
            return false;
        }
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<List<Integer>> petrolpumps = new ArrayList<>();

        IntStream.range(0, n).forEach(i -> {
            try {
                petrolpumps.add(
                    Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .map(Integer::parseInt)
                        .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        int result = Result.truckTour(petrolpumps);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
