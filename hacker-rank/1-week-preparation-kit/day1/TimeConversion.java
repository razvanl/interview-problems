// Given a time in -hour AM/PM format, convert it to military (24-hour) time.

// Note: - 12:00:00AM on a 12-hour clock is 00:00:00 on a 24-hour clock.
// - 12:00:00PM on a 12-hour clock is 12:00:00 on a 24-hour clock.

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {
    private static String PM_FORMAT = "PM";

    /*
     * Complete the 'timeConversion' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static String timeConversion(String s) {
        String timeFormat = s.substring(s.length() - 2, s.length()).toUpperCase();
        int hour = Integer.valueOf(s.substring(0, 2));
        String minutesAndSeconds = s.substring(2, s.length() - 2);
        
        hour = PM_FORMAT.equals(timeFormat) ? convertPmToMilitary(hour) : convertAmToMilitary(hour);
        
        return String.format("%02d%s", hour, minutesAndSeconds);
    }
    
    private static int convertPmToMilitary(int hour) {
        return hour == 12 ? hour : hour + 12;
    }
    
    private static int convertAmToMilitary(int hour) {
        return hour == 12 ? 0 : hour;
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = bufferedReader.readLine();

        String result = Result.timeConversion(s);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
