// Given an array of integers, calculate the ratios of its elements that are 
// positive, negative, and zero. Print the decimal value of each 
// fraction on a new line with  places after the decimal.

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'plusMinus' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static void plusMinus(List<Integer> list) {
        double total = list.size();
        double positiveCount = count(list, e -> e > 0);
        double negativeCount = count(list, e -> e < 0);
        double zeroCount = count(list, e -> e == 0);

        System.out.println(String.format("%.6f", positiveCount / total));
        System.out.println(String.format("%.6f", negativeCount / total));
        System.out.println(String.format("%.6f", zeroCount / total));
    }

    private static double count(List<Integer> list, Predicate<Integer> predicate) {
        return list.stream().filter(predicate).count();
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Integer::parseInt)
            .collect(toList());

        Result.plusMinus(arr);

        bufferedReader.close();
    }
}
