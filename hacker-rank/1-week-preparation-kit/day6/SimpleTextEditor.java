// Implement a simple text editor. The editor initially contains an empty string, . Perform  operations of the following  types:

// append - Append string  to the end of .
// delete - Delete the last  characters of .
// print - Print the  character of .
// undo - Undo the last (not previously undone) operation of type  or , reverting  to the state it was in prior to that operation.

import java.io.*;
import java.util.*;

public class Solution {
    private static ArrayDeque<String> editorState = new ArrayDeque<>();
    
    private static void append(String w) {
        String lastState = editorState.peek();
        editorState.push(lastState + w);
    }
    
    private static void deleteLastKthChars(int k) {
        String lastState = editorState.peek();
        editorState.push(lastState.substring(0, lastState.length() - k));
    }
    
    private static void printKthChar(int k) {
        String lastState = editorState.peek();
        System.out.println(lastState.charAt(k - 1));
    }
    
    private static void undo() {
        editorState.pop();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        editorState.push("");
        
        int q = Integer.parseInt(scanner.nextLine());
        
        for (int i = 0; i < q; i++) {
            String[] command = scanner.nextLine().split(" ");
            
            switch (command[0]) {
                case "1":
                    append(command[1]);
                    break;
                case "2":
                    deleteLastKthChars(Integer.parseInt(command[1]));
                    break;
                case "3":
                    printKthChar(Integer.parseInt(command[1]));
                    break;
                case "4":
                    undo();
            }
        }
    }
}