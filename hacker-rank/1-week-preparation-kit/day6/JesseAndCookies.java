// Jesse loves cookies and wants the sweetness of some cookies to be greater than value k. 
// To do this, two cookies with the least sweetness are repeatedly mixed. 
// This creates a special combined cookie with:
// sweetness  Least sweet cookie   2nd least sweet cookie).

// This occurs until all the cookies have a sweetness .
// Given the sweetness of a number of cookies, determine the minimum number of operations required. 
// If it is not possible, return -1

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'cookies' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER k
     *  2. INTEGER_ARRAY A
     */

    public static int cookies(int k, List<Integer> c) {
        int steps = 0;
        PriorityQueue<Integer> sortedCookies = new PriorityQueue<>(c);
        boolean allAreGreater = allAreGreaterThan(k, sortedCookies);
        
        while (!allAreGreater && sortedCookies.size() >= 2) {
            steps++;
            makeCookiesSweeter(sortedCookies);
            allAreGreater = allAreGreaterThan(k, sortedCookies);
        }
        
        return allAreGreater ? steps : -1;
    }
    
    private static boolean allAreGreaterThan(int k, PriorityQueue<Integer> cookies) {
        return cookies.stream().allMatch(c -> c >= k);
    }
    
    private static void makeCookiesSweeter(PriorityQueue<Integer> cookies) {
        int a = cookies.poll();
        int b = cookies.poll();
        
        cookies.add(a + 2 * b);
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(firstMultipleInput[0]);

        int k = Integer.parseInt(firstMultipleInput[1]);

        List<Integer> A = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Integer::parseInt)
            .collect(toList());

        int result = Result.cookies(k, A);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
