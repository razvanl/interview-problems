// A queue is an abstract data type that maintains the order in which elements were added to it, 
// allowing the oldest elements to be removed from the front and new elements to be added to the rear. 
// This is called a First-In-First-Out (FIFO) data structure because the first element added to the queue 
// (i.e., the one that has been waiting the longest) is always the first one to be removed.

// A basic queue has the following operations:

// Enqueue: add a new element to the end of the queue.
// Dequeue: remove the element from the front of the queue and return it.
// In this challenge, you must first implement a queue using two stacks. 
// Then process  queries, where each query is one of the following  types:

// 1 x: Enqueue element  into the end of the queue.
// 2: Dequeue the element at the front of the queue.
// 3: Print the element at the front of the queue.

import java.io.*;
import java.util.*;

public class Solution {
    private static ArrayDeque<Integer> s1 = new ArrayDeque<>();
    private static ArrayDeque<Integer> s2 = new ArrayDeque<>();
    
    private static void enque(Integer e) {
        s1.push(e);
    }
    
    private static void deque() {
        if (s2.isEmpty()) {
            while (!s1.isEmpty()) {
                s2.push(s1.pop());
            }
        }
        if (!s2.isEmpty()) {
            s2.pop();
        }
    }
    
    private static void printFrontElement() {
        if (s2.isEmpty()) {
            while (!s1.isEmpty()) {
                s2.push(s1.pop());
            }
        }
        if (!s2.isEmpty()) {
            System.out.println(s2.peek());
        }
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfQueries = Integer.valueOf(scanner.nextLine());
        
        for (int i = 0; i < numberOfQueries; i++) {
            int ops = scanner.nextInt();
            
            switch (ops) {
                case 1: enque(scanner.nextInt());
                    break;
                case 2: deque();
                    break;
                case 3: printFrontElement();
                    break;
            }
        }
        
        scanner.close();
    }
}