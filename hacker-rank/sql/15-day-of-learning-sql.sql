-- Julia conducted a  days of learning SQL contest. 
-- The start date of the contest was March 01, 2016 and the end date was March 15, 2016.

-- Write a query to print total number of unique hackers who made at least
-- submission each day (starting on the first day of the contest), 
-- and find the hacker_id and name of the hacker who made maximum number of submissions each day. 
-- If more than one such hacker has a maximum number of submissions, print the lowest hacker_id. 
-- The query should print this information for each day of the contest, sorted by the date.

drop table hackers
drop table submissions

create table hackers (
	hacker_id int,
	name varchar(100)
)

create table submissions (
	submission_date date,
	submission_id int,
	hacker_id int,
	score int
)

insert into hackers(hacker_id, name)
values
(15758, 'Rose'),
(20703, 'Angela'),
(36396, 'Frank'),
(38289, 'Patrick'),
(44065, 'Lisa'),
(53473, 'Kimberly'),
(62529, 'Bonnie'),
(79722, 'Michael')

insert into submissions(submission_date, submission_id, hacker_id, score)
values
('2016-03-01', 8494, 20703, 0),
('2016-03-01', 22403, 53473, 15),
('2016-03-01', 23965, 79722, 60),
('2016-03-01', 30173, 36396, 70),
('2016-03-02', 34928, 20703, 0),
('2016-03-02', 38740, 15758, 60),
('2016-03-02', 42769, 79722, 25),
('2016-03-02', 44364, 79722, 60),
('2016-03-03', 45440, 20703, 0),
('2016-03-03', 49050, 36396, 70),
('2016-03-03', 50273, 79722, 5),
('2016-03-04', 50344, 20703, 0),
('2016-03-04', 51360, 44065, 90),
('2016-03-04', 54404, 53473, 65),
('2016-03-04', 61533, 79722, 45),
('2016-03-05', 72852, 20703, 0),
('2016-03-05', 74546, 38289, 0),
('2016-03-05', 76487, 62529, 0),
('2016-03-05', 82439, 36396, 10),
('2016-03-05', 90008, 36396, 40),
('2016-03-06', 90404, 20703, 0)

with
submission_per_day as (
	select
		submission_date,
		hacker_id,
		count(submission_id) as counter
	from
		submissions
	where
		submission_date between '2016-03-01' and '2016-03-15'
	group by
		submission_date,
		hacker_id
),
continuous_submissions as (
	select
			submission_date,
			hacker_id
	from
			submission_per_day
	where
			submission_date = '2016-03-01'
	union all
	select 
			spd.submission_date,
			spd.hacker_id
	from
			continuous_submissions as cs
	join
			submission_per_day as spd
	on 
			dateadd(day, 1, cs.submission_date) = spd.submission_date
			and cs.hacker_id = spd.hacker_id
),
hacker_counter_per_day as (
	select 
			submission_date,
			count(hacker_id) as hacker_counter
	from
			continuous_submissions
	group by
		submission_date
),
max_submission as (
	select
		submission_date,
		hacker_id,
		counter,
		row_number() over(partition by submission_date
	order by
		counter desc,
		hacker_id asc) as rank
	from
		submission_per_day
)
select
	ms.submission_date,
	hcpd.hacker_counter,
	ms.hacker_id,
	h.name
from
	max_submission as ms
join
    hackers as ha
on
	ms.hacker_id = h.hacker_id
join
	hacker_counter_per_day hcpd
on 
	ms.submission_date = hcpd.submission_date
where 
	rank = 1
order by
	ms.submission_date
