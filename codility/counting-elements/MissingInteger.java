// Write a function:

// class Solution { public int solution(int[] A); }

// that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

// For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

// Given A = [1, 2, 3], the function should return 4.

// Given A = [−1, −3], the function should return 1.

// Write an efficient algorithm for the following assumptions:

// N is an integer within the range [1..100,000];
// each element of array A is an integer within the range [−1,000,000..1,000,000].

// Using Array
class Solution {
    public int solution(int[] A) {
        int[] occ = new int[1_000_000];

        for (int el : A) {
            if (el > 0) {
                occ[el - 1]++; 
            }
        }

        for (int i = 1; i <= A.length + 1; i++) {
            if (occ[i - 1] == 0) {
                return i;
            }
        }
        return A.length + 1;
    }
}

// Using Set
class Solution {
    public int solution(int[] A) {
        Set<Integer> positives = new HashSet<>();

        for (int el : A) {
            if (el > 0) {
                positives.add(el);
            }
        }

        for (int i = 1; i <= A.length + 1; i++) {
            if (!positives.contains(i)) {
                return i;
            }
        }
        return A.length + 1;
    }
}
