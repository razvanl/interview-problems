import java.util.*;
import java.lang.*;
import java.io.*;

class GfG {
  static class Graph {
    private int nrVertices;
    private int nrEdges;
    private List<Integer>[] edges;

    public Graph(int nrVertices, int nrEdges) {
      this.nrVertices = nrVertices;
      this.nrEdges = nrEdges;
      edges = new List[nrVertices];
      for (int i = 0; i < nrVertices; i++) {
        edges[i] = new LinkedList<Integer>();
      }
    }

    public void addEdge(int a, int b) {
      edges[a].add(b);
    }

    private int find(int node, int[] parents) {
      if (parents[node] == -1) {
        return node;
      } else {
        return find(parents[node], parents);
      }
    }

    private void union(int a, int b, int[] parents) {
      int pa = find(a, parents);
      int pb = find(b, parents);
      parents[pa] = pb;
    }

    public boolean hasCycle() {
      int[] parents = new int[nrVertices];
      for (int i = 0; i < nrVertices; i++) {
        parents[i] = -1;
      }

      for (int a = 0; a < nrVertices; a++) {
        for (int i = 0; i < edges[a].size(); i++) {
          int b = edges[a].get(i);
          int pa = find(a, parents);
          int pb = find(b, parents);
          if (pa == pb) {
            return true;
          }
          union(a, b, parents);
        }
      }
      return false;
    }
  }

  public static void main(String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int v = keyboard.nextInt();
    int e = keyboard.nextInt();
    Graph graph = new Graph(v, e);

    for (int i = 0; i < e; i++) {
      int a = keyboard.nextInt();
      int b = keyboard.nextInt();
      graph.addEdge(a, b);
    }

    System.out.println("Has cycle = " + graph.hasCycle());
  }
}
