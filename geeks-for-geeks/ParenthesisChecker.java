/*
Given an expression string exp, examine whether the pairs and the orders of “{“
,”}”,”(“,”)”,”[“,”]” are correct in exp.
For example, the program should print 'balanced' for exp = “[()]{}{[()()]()}”
and 'not balanced' for exp = “[(])”



Input:

The first line of input contains an integer T denoting the number of test cases.
Each test case consist of a string of expression, in a separate line.

Output:

Print 'balanced' without quotes if pair of parenthesis are balanced else print
 'not balanced' in a separate line.


Constraints:

1 ≤ T ≤ 100
1 ≤ |s| ≤ 100


Example:

Input:
3
{([])}
()
()[]

Output:
balanced
balanced
balanced
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static boolean isExpectedChar(Deque<Character> deque, Character op) {
    if (deque.isEmpty()) {
      return false;
    }
    else {
      return deque.pop().equals(op);
    }
  }
  static boolean isBalanced(String expression) {
    Deque<Character> deque = new ArrayDeque<Character>();
    for (char op : expression.toCharArray()) {
      switch (op) {
        case '{' :
        case '(' :
        case '[' : deque.push(op);
          break;
        case '}' : if (!isExpectedChar(deque, '{')) return false;
          break;
        case ')' : if (!isExpectedChar(deque, '(')) return false;
          break;
        case ']' : if (!isExpectedChar(deque, '[')) return false;
          break;
      }
    }

    return deque.isEmpty();
  }

	public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
		int t = Integer.valueOf(keyboard.next());

		for (int i = 0; i < t; i++) {
			String expression = keyboard.next();
			String answer = (isBalanced(expression)) ? "balanced" : "not balanced";
      System.out.println(answer);
		}
	}
}
