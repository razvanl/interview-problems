/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static int[][] readMatrix(Scanner keyboard, int n) {
      int[][] m = new int[n][n];
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          m[i][j] = keyboard.nextInt();
        }
      }
      return m;
  }

  static void printSpiral(int[][] m) {
    int n = m.length;
    for (int i = 0; i < (n + 1) / 2; i++) {
      for (int j = i; j < n - i; j++) {
        System.out.print(m[i][j] + " ");
      }
      for (int j = i + 1; j < n - i; j++) {
        System.out.print(m[j][n - i - 1] + " ");
      }
      for (int j = n - i - 2; j >= i; j--) {
        System.out.print(m[n - 1 - i][j] + " ");
      }
      for (int j = n - i - 2; j >= i + 1; j--) {
        System.out.print(m[j][i] + " ");
      }
    }
    System.out.println();
  }

	public static void main (String[] args) {
	   Scanner keyboard = new Scanner(System.in);
     int t = keyboard.nextInt();
     int n = 4;

     while (t-- > 0) {
       printSpiral(readMatrix(keyboard, n));
     }
	}
}
