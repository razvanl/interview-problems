/*package whatever //do not write package name here */
/*
Given two integers ‘L’ and ‘R’, write a program that finds the count of numbers
 having prime number of set bits in their binary representation in the range
 [L, R].

Input:
The first line consists of an integer T i.e number of test cases. The first and
 only line of each test case consists of two integers L and R. 

Output:
Print the required output.

Constraints:
1<=T<=100
1<=L<=R<=100000

Example:
Input:
2
6 10
10 15

Output:
4
5

*/

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static int countSetBits(int number) {
    int mask = 1;
    int setBits = 0;

    while (number != 0) {
      if ((number & mask) == 1) {
        setBits++;
      }
      number = number >> 1;
    }
    return setBits;
  }

  static boolean isPrime(int number) {
    if (number == 1) {
      return false;
    }

    for (int i = 2; i < number; i++) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }

  static int countPrimeNumberOfSetBits(int left, int right) {
    int count = 0;
    for (int i = left; i <= right; i++) {
      if (isPrime(countSetBits(i))) {
        count++;
      }
    }
    return count;
  }

	public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int nrTests =  keyboard.nextInt();

    for (int i = 0; i < nrTests; i++) {
      int left = keyboard.nextInt();
      int right = keyboard.nextInt();
      System.out.println(countPrimeNumberOfSetBits(left, right));
    }
	}
}
