/*package whatever //do not write package name here */
/*
Given a connected acyclic graph with N nodes and N-1 edges, find out the pair
of nodes that are at even distance from each other.

Input:

The first line of input contains an integer T denoting the number of test cases.

First line of each test case contains a single integer N denoting the number of
 nodes in graph.

Second line of each test case contains N-1 pair of nodes xi , yi denoting that
 there is an edges between them.


Output:

For each test case output a single integer denoting the pair of nodes which are
 at even distance.


Constraints:

1<=T<=10

1<=N<=10000

1<=xi,yi<=N

Example

Input

1

3

1 2 2 3

Output

1

Explanation:

Only such pair is (1,3) where distance b/w them is 2
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	static class Graph {
		private int vertices;
		private List<Integer> edges[];

		public Graph(int vertices) {
			this.vertices = vertices;
			edges = new List[vertices];
			for (int i = 0; i < vertices; i++) {
				edges[i] =  new ArrayList<Integer>();
			}
		}

		public void addEdge(int a, int b) {
			edges[a - 1].add(b - 1);
		}

		private void dfsUtil(int node, boolean[] visited, int[] distance, int countDist) {
			visited[node] = true;
			distance[node] = countDist;
			for (Integer nextNode : edges[node]) {
				if (!visited[nextNode]) {
					dfsUtil(nextNode, visited, distance, countDist + 1);
				}
			}
		}

		int[] distances() {
			boolean[] visited = new boolean[vertices];
			int[] distance = new int[vertices];
			dfsUtil(0, visited, distance, 0);
			return distance;
		}
	}

	public static int nodesAtEvenDistance(int[] distance) {
		int odd = 0;
		int even = 0;

		for (int i = 0; i < distance.length; i++) {
			if (distance[i] % 2 == 0) {
				even++;
			} else {
				odd++;
			}
		}

		return (odd * (odd - 1) / 2) + (even * (even - 1) / 2);
	}

	public static void main (String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int t = keyboard.nextInt();

		while (t-- > 0) {
			int n = keyboard.nextInt();
			Graph graph = new Graph(n);
			for (int i = 0; i < n - 1 ; i++) {
				int a = keyboard.nextInt();
				int b = keyboard.nextInt();
				graph.addEdge(a, b);
				graph.addEdge(b, a);
			}
			System.out.println(nodesAtEvenDistance(graph.distances()));
		}
	}
}
