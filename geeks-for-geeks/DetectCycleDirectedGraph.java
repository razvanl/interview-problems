import java.util.*;
import java.lang.*;
import java.io.*;

class GfG {
  static class Graph {
    private int vertices;
    private List<Integer>[] edges;

    public Graph(int vertices) {
      this.vertices = vertices;
      edges = new List[vertices];
      for (int i = 0; i < vertices; i++) {
        edges[i] = new LinkedList<Integer>();
      }
    }

    public void addEdge(int a, int b) {
      edges[a].add(b);
    }

    public boolean hasCycle() {
      boolean[] visited = new boolean[vertices];
      boolean[] recStack = new boolean[vertices];
      return hasCycle(0, visited,  recStack);
    }

    private boolean  hasCycle(int node, boolean[] visited, boolean[] recStack) {
      if (visited[node] && recStack[node]) {
        return true;
      } else {
        visited[node] = true;
        recStack[node] = true;
        for (Integer nextNode : edges[node]) {
          if (hasCycle(nextNode, visited, recStack)) {
            return true;
          }
        }
        recStack[node] = false;
        return false;
      }
    }
  }

  public static void main(String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int v = keyboard.nextInt();
    int nrEdges = keyboard.nextInt();
    Graph graph = new Graph(v);

    for (int i = 0; i < nrEdges; i++) {
      int a = keyboard.nextInt();
      int b = keyboard.nextInt();
      graph.addEdge(a, b);
    }

    System.out.println("Has cycle = " + graph.hasCycle());
  }
}
