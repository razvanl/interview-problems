/*package whatever //do not write package name here */
/*
Given a Matrix of size M x N. Your task is to print the matrix K times left
rotated.

Input:
First line consists of T test cases. First line of every test case consists of
3 integers, M, N, K. Second line of every test case consists of M*N spaced
integers.

Output:
Single line output, print the M*N spaced integer rotated K times.

Constraints:
1<=T<=100
1<=M,N,K<=100

Example:
Input:
1
2 2 1
1 2 3 4
Output:
2 1 4 3
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	static int[] readLine(int n, Scanner keyboard) {
		int[] line = new int[n];
		for (int t = 0; t < n; t++) {
			line[t] = keyboard.nextInt();
		}
		return line;
	}

	static int[] rotateKPosition(int[] line, int k) {
		int length = line.length;
		int[] rotated = new int[length];

		for (int i = length - 1; i >= 0; i--) {
				int index = ((i - k) < 0) ? (i - k + length) : (i - k);
				rotated[index] = line[i];
		}
		return rotated;
	}

	static String printLine(int[] line, String separator) {
		for (int i = 0; i < line.length; i++) {
			System.out.print(separator + line[i]);
			separator = " ";
		}
		return separator;
	}

	static void rotateMatrix(int m, int n, int k, Scanner keyboard) {
		String separator = "";
		for (int j = 0; j < m; j++) {
			int[] line = readLine(n, keyboard);
			separator = printLine(rotateKPosition(line, k), separator);
		}
		System.out.println();
	}

	public static void main (String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int nrTests = keyboard.nextInt();

		for (int i = 0; i < nrTests; i++) {
			int m = keyboard.nextInt();
			int n = keyboard.nextInt();
			int k = keyboard.nextInt();
			rotateMatrix(m, n, k, keyboard);
		}
	}
}
