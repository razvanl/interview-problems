/*package whatever //do not write package name here */
/*
Given two strings a and b print whether they contain any common subsequence
(non empty) or not.

Input:
The first line contains an integer T denoting number of testcases. Each of
the next T lines contains two strings a and b.

Output:
Print 1 if they have a common subsequence (non empty) else 0.

Constraints:
1<=T<=10^5
1<= |a|=|b| <=30
a and b consist of uppercase English letters ('A'....'Z')

Example:
Input:
1
ABEF CADE
Output:
1

Explanation:
AE is a subsequence of both the string so the answer is 1.
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static class FastReader {
    private BufferedReader reader;
    private StringTokenizer tokenizer;

    public FastReader() {
      reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public String next() {
      while (tokenizer == null || !tokenizer.hasMoreElements()) {
        try {
          tokenizer = new StringTokenizer(reader.readLine());
        } catch (IOException  e) {
            e.printStackTrace();
        }
      }
      return tokenizer.nextToken();
    }

    public int nextInt() {
      return Integer.parseInt(next());
    }
  }

  static int common(String s1, String s2) {
    for (int i = 0; i < s1.length(); i++) {
      if (s2.contains(new Character(s1.charAt(i)).toString())) {
        return 1;
      }
    }
    return 0;
  }

	public static void main (String[] args) {
    FastReader reader = new FastReader();
    int nrTests = reader.nextInt();

    for (int i = 0; i < nrTests; i++) {
      String s1 = reader.next();
      String s2 = reader.next();
      System.out.println(common(s1, s2));
    }
	}
}
