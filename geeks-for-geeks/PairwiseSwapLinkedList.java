/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static class Node {
    int value;
    Node next;
    public Node(int value) {
      this.value = value;
    }
  }

  static class LinkedList {
    Node head;

    public LinkedList(Node head) {
      this.head = head;
    }

    public LinkedList() {
    }

    public void add(Node node) {
      if (head == null) {
        head = node;
      }
      else {
        Node it = head;
        while (it.next != null) {
          it = it.next;
        }
        it.next = node;
      }
    }

    public void print() {
      Node it = head;
      String delimiter = "";

      while (it != null) {
        System.out.print(delimiter + it.value);
        delimiter = " ";
        it = it.next;
      }
      System.out.println();
    }

    public void pairwiseSwap() {
      Node curr = head;
      while (curr != null && curr.next != null) {
        int aux = curr.value;
        curr.value = curr.next.value;
        curr.next.value = aux;
        curr = curr.next.next;
      }
    }
  }

	public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int n = keyboard.nextInt();
    LinkedList list = new LinkedList();

    while (n-- > 0) {
      list.add(new Node(keyboard.nextInt()));
    }

    list.print();
    list.pairwiseSwap();
    list.print();
	}
}
