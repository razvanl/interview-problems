/*package whatever //do not write package name here */
/*
Given a string, print all permutations of a given string.

Input:

The first line of input contains an integer T denoting the number of test cases.
Each test case contains a single string in capital letter.

Output:

Print all permutations of a given string with single space and all permutations
 should be in lexicographically increasing order.

Constraints:

1 ≤ T ≤ 10
1 ≤ size of string ≤ 5

Example:

Input:
2
ABC

ABSG

Output:
ABC ACB BAC BCA CAB CBA

ABGS ABSG AGBS AGSB ASBG ASGB BAGS BASG BGAS BGSA BSAG BSGA GABS GASB GBAS GBSA
 GSAB GSBA SABG SAGB SBAG SBGA SGAB SGBA
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static List<String> permutations(String string, int left, int right) {
    if (left == right) {
      return Arrays.asList(string);
    }
    else {
      ArrayList<String> perms = new ArrayList<>();
      for (int i = left; i <= right; i++) {
        String swapped = swapCharacters(string, left, i);
        perms.addAll(permutations(swapped, left + 1, right));
      }
      return perms;
    }
  }

  static String swapCharacters(String string, int pos1, int pos2) {
    char[] chars = string.toCharArray();
    char aux = chars[pos1];
    chars[pos1] = chars[pos2];
    chars[pos2] = aux;
    return String.valueOf(chars);
  }

  static void printSolution(List<String> perms) {
    String delimiter = "";
    for (int i = 0; i < perms.size(); i++) {
      System.out.print(delimiter + perms.get(i));
      delimiter = " ";
    }
    System.out.println();
  }

	public static void main (String[] args) {
	   Scanner keyboard = new Scanner(System.in);
     int t = Integer.valueOf(keyboard.next());

     for (int i = 0; i < t; i++) {
       String string = keyboard.next();
       List<String> perms = permutations(string, 0, string.length() - 1);
       Collections.sort(perms);
       printSolution(perms);
     }
	}
}
