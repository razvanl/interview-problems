import java.util.*;
import java.lang.*;
import java.io.*;

class MaxProduct {
  static int findKElement(int[] array, int k, int low, int high) {
    int pivotIndex = low;
    int pivot = array[high];

    if (low >= high) {
      return array[pivotIndex];
    }
    for (int j = 0; j < high; j++) {
      if (array[j] >= pivot) {
        swap(array, pivotIndex, j);
        pivotIndex++;
      }
    }
    swap(array, pivotIndex, high);
    if (k > pivotIndex) {
      return findKElement(array, k, pivotIndex + 1, high);
    }
    else if (k < pivotIndex) {
      return findKElement(array, k, low, pivotIndex - 1);
    }
    else {
      return array[pivotIndex];
    }
  }

  static void swap(int[] array, int i, int j) {
    int aux = array[i];
    array[i] = array[j];
    array[j] = aux;
  }

  static int maxProduct(int[] array) {
    int maxProduct = 1;

    for (int i = 0; i <= 2; i++) {
      maxProduct *= findKElement(array, i, 0, array.length - 1);
    }
    return maxProduct;
  }

  public static void main(String[] args) {
    int[] array = {2, 10, 3, 2, 8, 11, 22};
    System.out.println(maxProduct(array));
  }
}
