import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	static String reverse(String word) {
		char[] chars = word.toCharArray();
		int start = 0;
		int stop = chars.length - 1;

		while (start < stop) {
			char aux = chars[start];
			chars[start] = chars[stop];
			chars[stop] = aux;
			start++;
			stop--;
		}

		return new String(chars);
	}

	public static void main (String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int t = Integer.parseInt(keyboard.next());

		while (t-- > 0) {
			String word = keyboard.next();
			System.out.println(reverse(word));
		}
	}
}
