/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
    private static int[][] readMatrix(Scanner keyboard, int n) {
        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = keyboard.nextInt();
            }
        }
        return matrix;
    }
    
    private static void printDiagonally(int[][] matrix, int n) {
        for (int step = 0; step < n ; step++) {
            for (int i = 0; i <= step; i++) {
                int j = step - i;
                System.out.print(matrix[i][j] + " ");
            }
        }
        
        for (int step = 1; step < n; step++) {
            int j = n - 1;
            for (int i = step; i <= n - 1; i++) {
                System.out.print(matrix[i][j] + " ");
                j--;
            }    
        }
    }
    
	public static void main (String[] args) {
	    Scanner keyboard = new Scanner(System.in);
        int t = keyboard.nextInt();
        
        while (t > 0) {
            t--;
            int n = keyboard.nextInt();
            int[][] matrix = readMatrix(keyboard, n);
            printDiagonally(matrix, n);
            System.out.println();
        }       
	}
}