/*This is a function problem.You only need to complete the function given below*/
/*Complete the function below*/
class GfG {
    public static void convertFive(int n) {
        int result = 0;
        int i = 1;

        while (n > 0) {
            int digit = n % 10;
            digit = digit == 0 ? 5 : digit;
            result = result + digit * i;
            i *= 10;
            n = n / 10;
        }

        System.out.println(result);
    }
}