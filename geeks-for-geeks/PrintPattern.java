/*package whatever //do not write package name here */
/*
Input:
The first line of the input contains a single integer T, denoting the number of
test cases. Then T test cases follow. Each test case contains one lines of
input denoting the value of n.

Output:
For each test case, Print the mentioned pattern.

Constraints:
1<=T<=100
1<=N<=9

Example:

Input:
2
3
4

Output:
33333
32223
32123
33333
4444444
4333334
4322234
4321234
4322234
4333334
4444444
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
    static void printPattern(int nr) {
        int length = 2 * nr - 1;
        int[] pattern = new int[length];

        for (int i = 0; i < nr; i++) {
            for (int j = i; j < length - i; j++) {
                pattern[j] = nr - i;
            }
            printArray(pattern);
        }

        for (int i = 1; i <= nr - 1; i++) {
            for (int j = nr - i; j < nr + i; j++) {
                pattern[j] = i + 1;
            }
            printArray(pattern);
        }
    }

    static void printArray(int[] pattern) {
        for (int i = 0; i < pattern.length; i++) {
            System.out.print(pattern[i]);
        }
        System.out.println();
    }

	public static void main (String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int n = keyboard.nextInt();
        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            numbers.add(keyboard.nextInt());
        }

        for (Integer number : numbers) {
            printPattern(number);
        }
	}
}
