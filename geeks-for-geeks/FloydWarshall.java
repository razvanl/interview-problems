/*package whatever //do not write package name here */
/*
The problem is to find shortest distances between every pair of vertices in a
given edge weighted directed Graph.

Input:
The first line of input contains an integer T denoting the no of test cases .
Then T test cases follow . The first line of each test case contains an integer
 V denoting the size of the adjacency matrix  and in the next line are V*V
 space separated values of the matrix (graph) .

Output:
For each test case output will be V*V space separated integers where the i-jth
 integer denote the shortest distance of ith vertex from jth vertex.

Constraints:
1<=T<=20
1<=V<=20
-1000<=graph[][]<=1000

Example:
Input
2
2
0 25 25 0
3
0 1 43 1 0 6 43 6 0

Output
0 25 25 0
0 1 7 1 0 6 7 6 0
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  public static int[][] readMatrix(int n, Scanner keyboard) {
    int[][] m = new int[n][n];
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        m[i][j] = keyboard.nextInt();
      }
    }
    return m;
  }

  public static void floydWarshall(int n, int[][] m) {
    for (int k = 0; k < n; k++) {
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          if (m[i][k] + m[k][j] < m[i][j]) {
            m[i][j] = m[i][k] + m[k][j];
          }
        }
      }
    }
  }

  public static void printMatrix(int n, int[][] m) {
    String delimiter = "";
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.print(delimiter + m[i][j]);
        delimiter = " ";
      }
    }
    System.out.println();
  }

	public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int t = keyboard.nextInt();

    while (t-- > 0) {
      int n = keyboard.nextInt();
      int[][] m = readMatrix(n, keyboard);
      floydWarshall(n, m);
      printMatrix(n, m);
    }
	}
}
