/*package whatever //do not write package name here */
/*
Given a string, print all permutations of a given string.

Input:

The first line of input contains an integer T denoting the number of test cases.
Each test case contains a single string in capital letter.

Output:

Print all permutations of a given string with single space and all permutations
 should be in lexicographically increasing order.

Constraints:

1 ≤ T ≤ 10
1 ≤ size of string ≤ 5

Example:

Input:
2
ABC

ABSG

Output:
ABC ACB BAC BCA CAB CBA

ABGS ABSG AGBS AGSB ASBG ASGB BAGS BASG BGAS BGSA BSAG BSGA GABS GASB GBAS GBSA
 GSAB GSBA SABG SAGB SBAG SBGA SGAB SGBA 
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static List<String> permutations(String string) {
    if (string.isEmpty()) {
      return new ArrayList<>();
    }
    else {
      ArrayList<String> perms = new ArrayList<>();
      for (int i = 0; i < string.length(); i++) {
        String removedChar = string.substring(0, i) + string.substring(i + 1);
        List<String> subPerms = permutations(removedChar);
        String oneLetterString = String.valueOf(string.charAt(i));
        if (subPerms.isEmpty()) {
          perms.add(oneLetterString);
        }
        else {
          for (int j = 0; j < subPerms.size(); j++) {
            perms.add(oneLetterString + subPerms.get(j));
          }
        }
      }
      return perms;
    }
  }

  static void printSolution(List<String> perms) {
    String delimiter = "";
    for (int i = 0; i < perms.size(); i++) {
      System.out.print(delimiter + perms.get(i));
      delimiter = " ";
    }
    System.out.println();
  }

	public static void main (String[] args) {
	   Scanner keyboard = new Scanner(System.in);
     int t = Integer.valueOf(keyboard.next());

     for (int i = 0; i < t; i++) {
       String string = keyboard.next();
       char[] letters = string.toCharArray();
       Arrays.sort(letters);
       String sortedString = new String(letters);
       printSolution(permutations(sortedString));
     }
	}
}
