/*package whatever //do not write package name here */
/*
Rahul and Ankit are the only two waiters in Royal Restaurant. Today, the
restaurant received N orders. The amount of tips may differ when handled by
different waiters, if Rahul takes the ith order, he would be tipped Ai rupees
and if Ankit takes this order, the tip would be Bi rupees.
In order to maximize the total tip value they decided to distribute the order
among themselves. One order will be handled by one person only. Also, due to
time constraints Rahul cannot take more than X orders and Ankit cannot take
more than Y orders. It is guaranteed that X + Y is greater than or equal to N,
which means that all the orders can be handled by either Rahul or Ankit. Find
out the maximum possible amount of total tip money after processing all the
orders.


Input:

•    The first line contains one integer, number of test cases.
•    The second line contains three integers N, X, Y.
•    The third line contains N integers. The ith integer represents Ai.
•    The fourth line contains N integers. The ith integer represents Bi.



Output:
Print a single integer representing the maximum tip money they would receive.


Constraints:
1 ≤ N ≤ 105
1 ≤ X, Y ≤ N; X + Y ≥ N
1 ≤ Ai, Bi ≤ 104



Example:

Input:

2
5 3 3
1 2 3 4 5
5 4 3 2 1
8 4 4
1 4 3 2 7 5 9 6
1 2 3 6 5 4 9 8



Output:

21
43
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class Tips {
  private int x;
  private int y;
  private int diff;

  public Tips(int x, int y, int diff) {
    this.x = x;
    this.y = y;
    this.diff = Math.abs(diff);
  }

  public int getX() { return x; }

  public int getY() { return y; }

  public int getDiff() { return diff; }

  public String toString() { return "(" + x + ", " + y + ")"; }
}

class GFG {
  static int[] readElements(Scanner keyboard, int n) {
    String[] numbers = keyboard.next().split(" ");
    int[] result = new int[n];

    for (int i = 0; i < n; i++) {
      result[i] = Integer.parseInt(numbers[i]);
    }
    return result;
  }

  static Tips[] createTipsArray(int[] xValues, int[] yValues) {
    Tips[] tips = new Tips[xValues.length];

    for (int i = 0; i < xValues.length; i++) {
      tips[i] = new Tips(xValues[i], yValues[i], xValues[i] - yValues[i]);
    }
    Arrays.sort(tips, (o1, o2) -> o2.getDiff() - o1.getDiff());
    return tips;
  }

  static int calculateMaxTip(Tips[] tips, int x, int y) {
    int maxTip = 0;

    for (int i = 0; i < tips.length; i++) {
      if (x == 0) {
        maxTip += tips[i].getY();
      }
      else if (y == 0) {
        maxTip += tips[i].getX();
      }
      else if (tips[i].getX() > tips[i].getY()) {
        maxTip += tips[i].getX();
        x--;
      }
      else if (tips[i].getX() < tips[i].getY()) {
        maxTip += tips[i].getY();
        y--;
      }
      else {
        maxTip += tips[i].getX();
      }
    }

    return maxTip;
  }

  public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
    keyboard.useDelimiter("\n");
    int nrTests = keyboard.nextInt();

    for (int i = 0; i < nrTests; i++) {
      String[] params = keyboard.next().split(" ");
      int n = Integer.parseInt(params[0]);
      int x = Integer.parseInt(params[1]);
      int y = Integer.parseInt(params[2]);

      int[] xValues = readElements(keyboard, n);
      int[] yValues = readElements(keyboard, n);
      int maxTip = calculateMaxTip(createTipsArray(xValues, yValues), x, y);
      System.out.println(maxTip);
    }
	}
}
