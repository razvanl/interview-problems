/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

/*
Given a positive integer N, find the Nth fibonacci number. Since the answer can
 be very large, print the answer modulo 1000000007.

Input:

The first line of input contains T denoting the number of testcases.Then each
of the T lines contains a single positive integer N.

Output:

Output the Nth fibonacci number.

Constraints:

1<=T<=200
1<=N<=1000

Example:

Input:
3
1
2
5

Output:
1
1
5
*/

class GFG {
  final static int MAX = 1000000007;

  static class FastReader
  {
    BufferedReader bufferedReader;
    StringTokenizer tokenizer;

    public FastReader() {
      bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }

    private String next() {
      while (tokenizer == null || !tokenizer.hasMoreElements()) {
        try {
            tokenizer = new StringTokenizer(bufferedReader.readLine());
        }
        catch (IOException  e) {
            e.printStackTrace();
        }
      }
      return tokenizer.nextToken();
    }

    int nextInt() {
        return Integer.parseInt(next());
    }
  }

  static int fibo(int n, int a, int b) {
    if (n == 0) return a;
    else if (n == 1) return b;
    else return fibo(n - 1, b, (a + b) % MAX);
  }

  static int fiboIt(int n) {
    int a = 1;
    int b = 1;

    for (int i = 2; i < n; i++) {
        int aux = a;
        a = (a + b) % MAX;
        b = aux;
    }

    return a;
  }

	public static void main (String[] args) {
		FastReader keyboard = new FastReader();
    int t = keyboard.nextInt();

    for (int i = 0; i < t; i++) {
      int n = keyboard.nextInt();
      System.out.println(fibo(n, 0, 1));
    }
	}
}
