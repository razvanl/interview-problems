import java.util.*;
import java.lang.*;
import java.io.*;

public class QuickSort {
    static int partition(int[] a, int start, int stop) {
      int i = start - 1;
      int pivot = a[stop];

      for (int j = start; j < stop; j++) {
        if (a[j] <= pivot) {
          i++;
          int aux = a[i];
          a[i] = a[j];
          a[j] = aux;
        }
      }

      int aux = a[stop];
      a[stop] = a[i];
      a[i] = aux;

      return i;
    }

    static void quickSort(int[] a, int start, int stop) {
      if (stop < start) {
        int pivot = partition(a, start, stop);
        quickSort(a, start, pivot - 1);
        quickSort(a, pivot + 1, stop);
      }
    }

    static int[] readNumbers(int n, Scanner keyboard) {
      int[] a = new int[n];
      for (int i = 0; i < n; i++) {
        a[i] = keyboard.nextInt();
      }
      return a;
    }

    static void printNumbers(int[] a) {
      String delimiter = "";
      for (int i = 0; i < a.length; i++) {
        System.out.print(delimiter + a[i]);
        delimiter = " ";
      }
      System.out.println();
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int t = keyboard.nextInt();

        while (t-- > 0) {
          int n = keyboard.nextInt();
          int[] a = readNumbers(n, keyboard);
          quickSort(a, 0, a.length - 1);
          printNumbers(a);
        }
    }
}
