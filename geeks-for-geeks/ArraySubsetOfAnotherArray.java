/**
 * Given two arrays: a1[0..n-1] of size n and a2[0..m-1] of size m. Task is to check whether a2[] is a subset of a1[] or not. 
 * Both the arrays can be sorted or unsorted. There can be duplicate elements.
 * 
 * Your Task:
 * You don't need to read input or print anything. Your task is to complete the function isSubset() which takes the array a1[], a2[], 
 * its size n and m as inputs and return "Yes" if arr2 is subset of arr1 else return "No" if arr2 is not subset of arr1.
 * Expected Time Complexity: O(max(n,m))
 * Expected Auxiliary Space: O(n)
 * Constraints:
 * 1 <= n,m <= 105
 * 1 <= a1[i], a2[j] <= 106
 * 
 */

//{ Driver Code Starts
//Initial Template for Java

//Initial Template for Java

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	public static void main(String[] args) throws IOException
	{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine().trim());
        while(t-->0)
        {
            StringTokenizer stt = new StringTokenizer(br.readLine());
            
            long n = Long.parseLong(stt.nextToken());
            long m = Long.parseLong(stt.nextToken());
            long a1[] = new long[(int)(n)];
            long a2[] = new long[(int)(m)];
            
            
            String inputLine[] = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a1[i] = Long.parseLong(inputLine[i]);
            }
            String inputLine1[] = br.readLine().trim().split(" ");
            for (int i = 0; i < m; i++) {
                a2[i] = Long.parseLong(inputLine1[i]);
            }
            
            
            Compute obj = new Compute();
            System.out.println(obj.isSubset( a1, a2, n, m));
            
        }
	}
}

// } Driver Code Ends


//User function Template for Java


class Compute {
    public String isSubset( long a1[], long a2[], long n, long m) {
        Map<Long, Long> map = new HashMap<>();
        
        for (int i = 0; i < n; i++) {
            Long elem = a1[i];
            map.put(elem, map.getOrDefault(elem, 0L) + 1);
        }
        
        for (int i = 0; i < m; i++) {
            Long searched = a2[i];
            Long occ = map.get(searched);
            
            if (occ == null || occ == 0) {
                return "No";
            }
            else {
                map.replace(searched, occ - 1);
            }
        }
        
        return "Yes";
    }
}