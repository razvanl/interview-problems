/*package whatever //do not write package name here */
/*
Given an array, print k largest elements from the array.  The output elements
should be printed in decreasing order.

Input:

The first line of input contains an integer T denoting the number of test cases.
The first line of each test case is N and k, N is the size of array and K is
the largest elements to be returned.
The second line of each test case contains N input C[i].

Output:

Print the k largest element in descending order.

Constraints:

1 ≤ T ≤ 100
1 ≤ N ≤ 100
K ≤ N
1 ≤ C[i] ≤ 1000

Example:

Input:
2
5 2
12 5 787 1 23
7 3
1 23 12 9 30 2 50

Output:
787 23
50 30 23
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	static int[] readElements(int n, Scanner keyboard) {
		int[] elems = new int[n];
		for (int i = 0; i < n; i++) {
			elems[i] = keyboard.nextInt();
		}
		return elems;
	}

	static void printKElems(int[] elems, int k) {
		int n = elems.length;
		String delimiter = "";
		for (int i = n - 1; i > n - 1 - k; i--) {
			System.out.print(delimiter + elems[i]);
			delimiter = " ";
		}
		System.out.println();
	}

	public static void main (String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int t = keyboard.nextInt();

		for (int i = 0; i < t; i++) {
			int n = keyboard.nextInt();
			int k = keyboard.nextInt();
			int[] elems = readElements(n, keyboard);
			Arrays.sort(elems);
			printKElems(elems, k);
		}

	}
}
