{
    import java.util.*;
    class Node
    {
        int data;
        Node next;
        
        Node(int d)
        {
            data = d;
            next = null;
        }
    }
    class Is_LinkedList_Palindrom
    {
         Node head;  
        
        /* Function to print linked list */
        void printList(Node head)
        {
            Node temp = head;
            while (temp != null)
            {
               System.out.print(temp.data+" ");
               temp = temp.next;
            }  
            System.out.println();
        }
        
     
        /* Inserts a new Node at front of the list. */
        public void addToTheLast(Node node) 
        {
            if (head == null) 
            {
                head = node;
            } else 
            {
               Node temp = head;
               while (temp.next != null)
               temp = temp.next;
               temp.next = node;
            }
        }
        
        public static void main(String args[])
        {
            Scanner sc = new Scanner(System.in);
            int t = sc.nextInt();
             
            while(t>0)
            {
                int n = sc.nextInt();
                //int k = sc.nextInt();
                Is_LinkedList_Palindrom llist = new Is_LinkedList_Palindrom();
                //int n=Integer.parseInt(br.readLine());
                int a1=sc.nextInt();
                Node head= new Node(a1);
                llist.addToTheLast(head);
                for (int i = 1; i < n; i++) 
                {
                    int a = sc.nextInt(); 
                    llist.addToTheLast(new Node(a));
                }
                
                Palindrome g = new Palindrome();
                if(g.isPalindrome(llist.head) == true)
                System.out.println(1);
            else
                System.out.println(0);
                t--;
            }
            
        }
    }
    
    }
    /*This is a function problem.You only need to complete the function given below*/
    /* Structure of class Node is
    class Node
    {
        int data;
        Node next;
        
        Node(int d)
        {
            data = d;
            next = null;
        }
    }*/
    class Palindrome
    {
        // Function to check if linked list is palindrome
        boolean isPalindrome(Node head) 
        {
            Deque<Integer> stack = new ArrayDeque<>();
            
            Node it = head;
            Node fastIt = head;
            
            if (it.next != null && it.next.next == null) {
                it = it.next;
                return head.data == it.data;
            }
            
            while (fastIt.next != null && fastIt.next.next != null) {
                stack.push(it.data);
                it = it.next;
                fastIt = fastIt.next.next;
            }
            
            if (fastIt.next != null && it.next != null) {
                stack.push(it.data);
            }
        
            it = it.next;
            
            
            while (it != null) {
                int data = stack.pop();
                if (data != it.data) {
                    return false;
                }
                it = it.next;
            }
            
            return true;
        }    
    }
