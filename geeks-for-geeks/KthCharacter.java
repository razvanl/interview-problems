/*package whatever //do not write package name here */
/*
Given a decimal number m. Convert it in binary string and apply n iterations,
in each iteration 0 becomes 01 and 1 becomes 10. Find kth character in the
string after nth iteration.

Input:
The first line consists of an integer T i.e number of test cases. The first and
 only line of each test case consists of three integers m,k,n. 

Output:
Print the required output.

Constraints:
1<=T<=100
1<=m<=50
1<=n<=10
0<=k<=|Length of final string|

Example:
Input:
2
5 5 3
11 6 4

Output:
1
1
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static String toBinary(int m) {
    StringBuilder binary = new StringBuilder();

    while (m != 0) {
      if ((m & 1) == 1) {
        binary.append("1");
      } else {
        binary.append("0");
      }
      m = m >> 1;
    }

    return binary.reverse().toString();
  }

  static int kthChar(String binary, int n, int k) {
      if (n == 0) {
        return Character.getNumericValue(binary.charAt(k));
      } else if (k % 2 == 0) {
        return kthChar(binary, n - 1, k / 2);
      } else {
        return 1 - kthChar(binary, n - 1, k / 2);
      }
  }

	public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int nrTests = keyboard.nextInt();

    for (int i = 0; i < nrTests; i++) {
      int m = keyboard.nextInt();
      int k = keyboard.nextInt();
      int n = keyboard.nextInt();
      System.out.println(kthChar(toBinary(m), n, k));
    }
	}
}
