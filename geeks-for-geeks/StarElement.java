/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

/*
Given an unsorted array. The task is to find all the star and super star
elements in the array. Star are those elements which are strictly greater than
all the elements on its right side. Super star are those elements which are
strictly greater than all the elements on its left and right side.

Note: Assume first element (A[0]) is greater than all the elements on its left
side, And last element (A[n-1]) is greater than all the elements on its right
side.

Input:
The first line of input contains an integer T denoting the number of test
cases. Then T test cases follow. Each test case consists of two lines. First
line of each test case contains an Integer N denoting size of array and the
second line contains N space separated elements.

Output:
For each test case, print the space separated star elements and then in new
line print super star elements. If no super star element present in array then
print "-1".

Constraints:
1<=T<=200
1<=N<=106
1<=A[i]<=106

Example:
Input:
2
6
4 2 5 7 2 1
3
8 6 5

Output:
7 2 1
7
8 6 5
8
*/

class GFG {
  static class FastReader
  {
    BufferedReader bufferedReader;
    StringTokenizer tokenizer;

    public FastReader() {
      bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }

    private String next() {
      while (tokenizer == null || !tokenizer.hasMoreElements()) {
        try {
            tokenizer = new StringTokenizer(bufferedReader.readLine());
        }
        catch (IOException  e) {
            e.printStackTrace();
        }
      }
      return tokenizer.nextToken();
    }

    int nextInt() {
        return Integer.parseInt(next());
    }
  }

  static int[] readLine(int n, FastReader keyboard) {
    int[] line = new int[n];
    for (int i = 0; i < n; i++) {
      line[i] = keyboard.nextInt();
    }
    return line;
  }

  static int[] findStarElements(int[] line) {
    int[] stars = new int[line.length + 1];
    int star = -1;
    int found = 1;
    int count = 1;

    for (int i = line.length - 1; i >= 0; i--) {
      if (star < line[i]) {
        stars[count++] = line[i];
        star = line[i];
        found = 1;
      } else if (star == line[i]) {
        found++;
      }
    }
    stars[0] = (found == 1) ? star : -1;

    return Arrays.copyOf(stars, count);
  }

  static void printStarElements(int[] stars) {
    String delimiter = "";
    for (int i = stars.length - 1; i >= 1; i--) {
      System.out.print(delimiter + stars[i]);
      delimiter = " ";
    }
    System.out.println();
    System.out.println(stars[0]);
  }

	public static void main (String[] args) {
    FastReader keyboard = new FastReader();
    int nrTests = keyboard.nextInt();

    for (int i = 0; i < nrTests; i++) {
      int n = keyboard.nextInt();
      printStarElements(findStarElements(readLine(n, keyboard)));
    }
  }
}
