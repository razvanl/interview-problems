/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static class Graph {
    int vertices;

    public Graph(int vertices) {
      this.vertices = vertices;
    }

    public int[] link(int node) {
      int[] nextNodes = new int[2];
      nextNodes[0] = (node + 1 <= vertices) ? node + 1 : -1;
      nextNodes[1] = (3 * node <= vertices) ? 3 * node : -1;
      return nextNodes;
    }

    private int getMinIndex(int[] distance, boolean[] visited) {
      int min = Integer.MAX_VALUE;
      int index = 1;
      for (int i = 1; i < distance.length; i++) {
        if ((min > distance[i]) && !visited[i]) {
          index = i;
          min = distance[i];
        }
      }
      return index;
    }

    public int shortestPath() {
      int[] distances = new int[vertices + 1];
      boolean[] visited = new boolean[vertices + 1];

      for (int i = 2; i <= vertices; i++) {
        distances[i] = Integer.MAX_VALUE;
      }

      for (int i = 0; i < vertices - 1; i++) {
        int node = getMinIndex(distances, visited);
        visited[node] = true;
        int[] links = link(node);
        for (int j = 0; j < links.length; j++) {
          int nextNode = links[j];
          if (nextNode != -1 && !visited[nextNode]
          && distances[node] + 1 < distances[nextNode]) {
            distances[nextNode] = distances[node] + 1;
          }
        }
      }

      return distances[vertices];
    }
  }

	public static void main (String[] args) {
	   Scanner keyboard = new Scanner(System.in);
     int t = keyboard.nextInt();

     while (t-- > 0) {
       int n = keyboard.nextInt();
       Graph graph = new Graph(n);
       System.out.println(graph.shortestPath());
     }
	}
}
