/*package whatever //do not write package name here */
/*
Given an array of size n-1 and given that there are numbers from 1 to n with
one missing, the missing number is to be found.

Input:

The first line of input contains an integer T denoting the number of test cases.
The first line of each test case is N.
The second line of each test case contains N-1 input C[i],numbers in array.

Output:

Print the missing number in array.

Constraints:

1 ≤ T ≤ 200
1 ≤ N ≤ 1000
1 ≤ C[i] ≤ 1000

Example:

Input
2
5
1 2 3 5
10
1 2 3 4 5 6 7 8 10

Output
4
9
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {

	static int[] readElements(int n, Scanner scanner) {
		int[] elems = new int[n];
		for (int i = 0; i < n - 1; i++) {
			elems[i] = scanner.nextInt();
		}
		return elems;
	}

	static int findMissingElement(int[] elems, int n) {
		int sum = 0;
		for (int i = 0; i < n - 1; i++) {
			sum += elems[i];
		}
		return (n * (n + 1) / 2) - sum;
	}

	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		int nrTests =  scanner.nextInt();

		for (int i = 0; i < nrTests; i++) {
			int n = scanner.nextInt();
			int[] elements = readElements(n, scanner);
			System.out.println(findMissingElement(elements, n));
		}
	}
}
