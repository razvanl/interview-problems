/*package whatever //do not write package name here */
/*
Given N * M string array of O's and X's
Return the number of 'X' total shapes. 'X' shape consists of one or more
adjacent X's (diagonals not included).

Example (1):

OOOXOOO
OXXXXXO
OXOOOXO

answer is 1 , shapes are  :
(i)     X
    X X X X
    X     X


Example (2):

XXX
OOO
XXX

answer is 2, shapes are
(i)  XXX

(ii) XXX

Input:
The first line of input takes the number of test cases, T.
Then T test cases follow. Each of the T test cases takes 2 input lines.
The first line of each test case have two integers N and M.The second line of N
 space separated strings follow which indicate the elements in the array.

Output:

Print number of shapes.

Constraints:

1<=T<=100

1<=N,M<=50

Example:

Input:
2
4 7
OOOOXXO OXOXOOX XXXXOXO OXXXOOO
10 3
XXO OOX OXO OOO XOX XOX OXO XXO XXX OOO

Output:
4
6
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static class Pair {
    private int i;
    private int j;

    Pair(int i, int j) {
      this.i = i;
      this.j = j;
    }

    public int getI() { return i; }

    public int getJ() { return j; }

    public List<Pair> addIfValid(int n, int m, int[][] figure,
      boolean[][] visited, List<Pair> neigh) {
      if (isValid(n, m, figure, visited)) {
        neigh.add(this);
      }
      return neigh;
    }

    private boolean isValid(int n, int m, int[][] figure, boolean[][] visited) {
      return (0 <= i) && (i < n) && (0 <= j) && (j < m) && !visited[i][j]
      && figure[i][j] == 1;
    }
  }

  static int[][] readFigure(int n, int m, Scanner keyboard) {
    int[][] figure = new int[n][m];
    for (int i = 0; i < n; i++) {
      String line = keyboard.next();
      for (int j = 0; j < m; j++) {
        if (line.charAt(j) == 'X') {
          figure[i][j] = 1;
        }
      }
    }
    return figure;
  }

  static int countFigures(int[][] figure) {
    int n = figure.length;
    int m = figure[0].length;
    boolean[][] visited = new boolean[n][m];
    int count = 0;

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        if (!visited[i][j] && figure[i][j] == 1) {
          count++;
          explore(i, j, figure, visited);
        }
      }
    }
    return count;
  }

  static void explore(int i, int j, int[][] figure, boolean[][] visited) {
    Queue<Pair> queue = new LinkedList<>();
    visited[i][j] = true;
    queue.add(new Pair(i, j));

    while (!queue.isEmpty()) {
      Pair node = queue.poll();
      List<Pair> nextNodes = findNextNodes(node.getI(), node.getJ(), figure, visited);
      for (Pair nextNode : nextNodes) {
        visited[nextNode.getI()][nextNode.getJ()] = true;
        queue.add(nextNode);
      }
    }
  }

  static List<Pair> findNextNodes(int i, int j, int[][] figure, boolean[][] visited) {
    int n = figure.length;
    int m = figure[0].length;
    List<Pair> neigh = new ArrayList<>();

    new Pair(i + 1, j).addIfValid(n, m, figure, visited, neigh);
    new Pair(i - 1, j).addIfValid(n, m, figure, visited, neigh);
    new Pair(i, j + 1).addIfValid(n, m, figure, visited, neigh);
    new Pair(i, j - 1).addIfValid(n, m, figure, visited, neigh);

    return neigh;
  }

	public static void main (String[] args) {
	   Scanner keyboard = new Scanner(System.in);
     int t = Integer.valueOf(keyboard.next());

     while (t-- > 0) {
       int n = Integer.valueOf(keyboard.next());
       int m = Integer.valueOf(keyboard.next());
       int[][] figure = readFigure(n, m, keyboard);
       System.out.println(countFigures(figure));
     }
	}
}
