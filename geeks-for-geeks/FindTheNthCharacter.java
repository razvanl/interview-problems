/*package whatever //do not write package name here */
/*
Given a binary string S . The task is to perform R iterations on string S, in
each iteration 0 becomes 01 and 1 becomes 10. Find the Nth character of the
string after performing R iterations.

Input:
The first line consists of an integer T i.e number of test cases. The first and
only line of each test case contains string S, and two Integers R and N
respectively.

Output:
For each test case print the Nth character of string S after Rth iterations in
new line.

Constraints:
1<=T<=100
1<=|string length|<=1000
1<=R<=20
0<=N<|Length of final string|

Example:
Input:
2
101 2 3
11 1 3

Output:
1
0
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
    static int nthCharacter(String number, int r, int n) {
        if (r == 0) {
            return Character.getNumericValue(number.charAt(n));
        }
        else if (n % 2 == 0) {
            return nthCharacter(number, r - 1, n / 2);
        } else {
            return 1 - nthCharacter(number, r - 1, n / 2);
        }
    }

    public static void main (String[] args) {
      Scanner keyboard = new Scanner(System.in);
      keyboard.useDelimiter("\n| ");
      int t = keyboard.nextInt();

      for(int i = 0; i < t; i++) {
        String number = keyboard.next();
        int r = keyboard.nextInt();
        int n = keyboard.nextInt();
        System.out.println(nthCharacter(number, r, n));
      }
    }
}
