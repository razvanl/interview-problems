/*package whatever //do not write package name here */
/*
Given N * M string array of O's and X's
Return the number of 'X' total shapes. 'X' shape consists of one or more
adjacent X's (diagonals not included).

Example (1):

OOOXOOO
OXXXXXO
OXOOOXO

answer is 1 , shapes are  :
(i)     X
    X X X X
    X     X


Example (2):

XXX
OOO
XXX

answer is 2, shapes are
(i)  XXX

(ii) XXX

Input:
The first line of input takes the number of test cases, T.
Then T test cases follow. Each of the T test cases takes 2 input lines.
The first line of each test case have two integers N and M.The second line of N
 space separated strings follow which indicate the elements in the array.

Output:

Print number of shapes.

Constraints:

1<=T<=100

1<=N,M<=50

Example:

Input:
2
4 7
OOOOXXO OXOXOOX XXXXOXO OXXXOOO
10 3
XXO OOX OXO OOO XOX XOX OXO XXO XXX OOO

Output:
4
6
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static class FigureAndVertices{
    private int[][] figure;
    private int vertices;

    public FigureAndVertices(int[][] figure, int vertices) {
      this.figure = figure;
      this.vertices = vertices;
    }

    public int[][] getFigure() { return figure; }

    public int getVertices() { return vertices; }
  }

  static class Graph {
    private int vertices;
    private List<Integer> edges[];

    public Graph(int vertices) {
      this.vertices = vertices;
      edges = new List[vertices];
      for (int i = 0; i < vertices; i++) {
        edges[i] = new ArrayList<>();
      }
    }

    public void addEdge(int a, int b) {
      edges[a].add(b);
    }

    public int countFigures() {
      boolean[] visited = new boolean[vertices];
      int countFig = 0;
      LinkedList<Integer> queue = new LinkedList<>();

      while (!areAllVisited(visited)) {
        int unvisited = getUnvisited(visited);
        queue.add(unvisited);
        visited[unvisited] = true;
        countFig++;
        while (!queue.isEmpty()) {
          int v = queue.poll();
          for (int i = 0; i < edges[v].size(); i++) {
            if (!visited[edges[v].get(i)]) {
              int newVertex = edges[v].get(i);
              queue.add(newVertex);
              visited[newVertex] = true;
            }
          }
        }
      }

      return countFig;
    }

    private int getUnvisited(boolean[] visited) {
      for (int i = 0; i < visited.length; i++) {
        if (!visited[i]) {
          return i;
        }
      }
      return -1;
    }

    private boolean areAllVisited(boolean[] visited) {
      for (int i = 0; i < visited.length; i++) {
        if (!visited[i]) {
          return false;
        }
      }
      return true;
    }
  }

  static FigureAndVertices readFigure(int n, int m, Scanner keyboard) {
    int[][] figure = new int[n][m];
    int vertices = 1;
    for (int i = 0; i < n; i++) {
      String line = keyboard.next();
      for (int j = 0; j < m; j++) {
        if (line.charAt(j) == 'X') {
          figure[i][j] = vertices++;
        }
      }
    }
    return new FigureAndVertices(figure, vertices - 1);
  }

  static Graph createGraph(int vertices, int[][] figure) {
    Graph graph = new Graph(vertices);

    for (int i = 0; i < figure.length; i++) {
      for (int j = 0; j < figure[0].length; j++) {
        if (isVertex(i, j, figure)) {
          int start = figure[i][j];
          addEdge(i, j + 1, start, graph, figure);
          addEdge(i, j - 1, start, graph, figure);
          addEdge(i + 1, j, start, graph, figure);
          addEdge(i - 1, j, start, graph, figure);
        }
      }
    }
    return graph;
  }

  static void addEdge(int a, int b, int start, Graph graph, int[][] figure) {
    if (isVertex(a, b, figure)) {
      int end = figure[a][b];
      graph.addEdge(start - 1, end - 1);
    }
  }

  static boolean isVertex(int i, int j, int[][] figure) {
    if (0 <= i && i < figure.length && 0 <= j && j < figure[0].length) {
      return figure[i][j] != 0;
    }
    return false;
  }

	public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int t = Integer.valueOf(keyboard.next());

    for (int i = 0; i < t; i++) {
      int n = Integer.valueOf(keyboard.next());
      int m = Integer.valueOf(keyboard.next());
      FigureAndVertices figAndVert = readFigure(n, m, keyboard);
      Graph graph = createGraph(figAndVert.getVertices(), figAndVert.getFigure());
      int[][] fig = figAndVert.getFigure();
      System.out.println(graph.countFigures());
    }
  }
}
