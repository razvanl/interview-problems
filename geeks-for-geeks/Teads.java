import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {
    private static Map<Integer, Set<Integer>> edges = new HashMap<>();

    static void addEdge(int xi, int yi) {
        Set<Integer> neighs = edges.computeIfAbsent(xi, node -> new HashSet<>());
        neighs.add(yi);
    }

    static boolean isLeaf(int node) {
        return edges.get(node).size() == 1;
    }

    static void removeNode(int node) {
        Set<Integer> neighs = edges.remove(node);
        for (Integer neigh : neighs) {
            edges.get(neigh).remove(node);
        }
    }

    static int minimalSteps() {
        int steps = 0;
        while (edges.size() > 1) {
            Set<Integer> toBeRemoved = new HashSet<>();
            for(Integer node : edges.keySet()) {
                if (isLeaf(node)) {
                    toBeRemoved.add(node);
                }
            }
            toBeRemoved.forEach(Solution::removeNode);
            steps++;
        }
        return steps;
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt(); // the number of adjacency relations
        for (int i = 0; i < n; i++) {
            int xi = in.nextInt(); // the ID of a person which is adjacent to yi
            int yi = in.nextInt(); // the ID of a person which is adjacent to xi
            addEdge(xi, yi);
            addEdge(yi, xi);
        }

        // The minimal amount of steps required to completely propagate the advertisement
        System.out.println(minimalSteps());
    }
}
