import java.util.*;
import java.lang.*;
import java.io.*;

class GfG {
  static class Edge implements Comparable<Edge> {
    int a;
    int b;
    int weight;

    public Edge(int a, int b, int weight) {
      this.a = a;
      this.b = b;
      this.weight = weight;
    }

    @Override
    public int compareTo(Edge other) {
      return this.weight - other.weight;
    }

    @Override
    public String toString() {
      return "(" + a + ", " + b + ")";
    }
  }

  static class Elem {
    int parent;
    int rank;

    public Elem(int parent, int rank) {
      this.parent = parent;
      this.rank = rank;
    }
  }

  static class Graph {
    private int nrVertices;
    private int nrEdges;
    private List<Edge> edges;

    public Graph(int nrVertices, int nrEdges) {
      this.nrVertices = nrVertices;
      this.nrEdges = nrEdges;
      edges = new LinkedList<>();
    }

    public void addEdge(int a, int b, int weight) {
      edges.add(new Edge(a, b, weight));
    }

    public int find(int a, Elem[] parents) {
      if (parents[a].parent != a) {
        parents[a].parent = find(parents[a].parent, parents);
      }

      return parents[a].parent;
    }

    public void union(int a, int b, Elem[] parents) {
      int pa = find(a, parents);
      int pb = find(b, parents);

      if (parents[pa].rank > parents[pb].rank) {
        parents[pb].parent = pa;
      }
      else if (parents[pa].rank < parents[pb].rank) {
        parents[pa].parent = pb;
      }
      else {
        parents[pb].parent = pa;
        parents[pb].rank++;
      }
    }

    public List<Edge> minmumSpanningTree() {
      List<Edge> mst = new LinkedList<>();
      Elem[] parents = new Elem[nrVertices];
      Collections.sort(edges);

      for (int i = 0; i < nrVertices; i++) {
        parents[i] = new Elem(i, 0);
      }

      for (Edge edge : edges) {
        int pa = find(edge.a, parents);
        int pb = find(edge.b, parents);

        if (pa != pb) {
          mst.add(edge);
          union(edge.a, edge.b, parents);
        }
      }

      return mst;
    }
  }

  public static void main(String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int v = keyboard.nextInt();
    int e = keyboard.nextInt();
    Graph graph = new Graph(v, e);

    for (int i = 0; i < e; i++) {
      int a = keyboard.nextInt();
      int b = keyboard.nextInt();
      int rank = keyboard.nextInt();
      graph.addEdge(a, b, rank);
    }
    System.out.println(graph.minmumSpanningTree());
  }
}
