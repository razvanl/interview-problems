/*package whatever //do not write package name here */
/*
Given an array A[] of n numbers and another number x, determine whether or not
there exist two elements in A whose sum is exactly x.

Input:

The first line of input contains an integer T denoting the number of test cases.
The first line of each test case is N and X,N is the size of array.
The second line of each test case contains N integers representing array
elements C[i].

Output:

Print "Yes" if there exist two elements in A whose sum is exactly x, else "No"
without quotes.

Constraints:

1 ≤ T ≤ 200
1 ≤ N ≤ 200
1 ≤ C[i] ≤ 1000

Example:

Input:
2
6 16
1 4 45 6 10 8
5 10
1 2 4 3 6

Output:
Yes
Yes
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static int[] readLine(int n, Scanner keyboard) {
		int[] elems = new int[n];
		for (int i = 0; i < n; i++) {
			elems[i] = keyboard.nextInt();
		}
		return elems;
	}

  static void findKeyPair(int[] elems, int x) {
    int left = 0;
    int n = elems.length;
    boolean found = false;
    while (left < n - 1) {
      for (int i = left + 1; i < n; i++) {
        if ((elems[left] + elems[i]) == x) {
          System.out.println("Yes");
          return;
        }
      }
      left++;
    }
    System.out.println("No");
  }

	public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
		int t = keyboard.nextInt();

		for (int i = 0; i < t; i++) {
			int n = keyboard.nextInt();
			int x = keyboard.nextInt();
			int[] elems = readLine(n, keyboard);
			findKeyPair(elems, x);
		}
	}
}
