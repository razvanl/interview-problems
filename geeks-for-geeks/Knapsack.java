// Using recursion
class Knapsack 
{ 
    // Returns the maximum value that can be put in a knapsack of capacity W 
    static int knapSack(int W, int wt[], int val[], int n) 
    { 
        Map<Integer, Map<Integer, Integer>> cache = new HashMap<>();
        return knapSack(W, wt, val, n, 0, cache);
    }
    
    static int knapSack(int W, int wt[], int val[], int n, 
                        int i, Map<Integer, Map<Integer, Integer>> cache) 
    {
        if (i == n) 
            return 0;
            
        if (!cache.containsKey(i)) cache.put(i, new HashMap<>());
        Integer cached = cache.get(i).get(W);
        if (cached != null) {
            return cached;
        }
            
        if (W - wt[i] < 0) {
            return knapSack(W, wt, val, n, i + 1, cache);
        }
        
        int valueToReturn = Math.max(
                knapSack(W - wt[i], wt, val, n, i + 1, cache) + val[i],
                knapSack(W, wt, val, n, i + 1, cache)
                );
        cache.get(i).put(W, valueToReturn);
        
        return valueToReturn;
    }
}


// Using dynamic programming
class Knapsack 
{ 
    // Returns the maximum value that can be put in a knapsack of capacity W 
    static int knapSack(int W, int wt[], int val[], int n) 
    { 
        int[][] cache = new int[n + 1][W + 1];
        
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= W; j++) {
                if (j - wt[i - 1] < 0) 
                    cache[i][j] = cache[i - 1][j];
                else 
                    cache[i][j] = Math.max(
                                cache[i - 1][j],
                                cache[i - 1][j - wt[i - 1]] + val[i - 1]
                            );
            }
        }
        
        return cache[n][W];
    }
}