/*package whatever //do not write package name here */
/*
Given an array of distinct integers, print all the pairs having positive value
and negative value of a number that exists in the array.

NOTE: If there is no such pair in the array , print "0".

Input:
The first line consists of an integer T i.e number of test cases. The first
line of each test case consists of an integer n.The next line of each test case
consists of n spaced integers.

Output:
Print all the required pairs in the increasing order of their absolute numbers.

Constraints:
1<=T<=100
1<=n<=1000
-1000<=a[i]<=1000

Example:
Input:
2
6
1 -3 2 3 6 -1
8
4 8 9 -4 1 -1 -8 -9

Output:
-1 1 -3 3
-1 1 -4 4 -8 8 -9 9
*/
import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
  static List<Integer> createPairs(TreeSet<Integer> orderedNum) {
    Iterator<Integer> negatives = orderedNum.descendingIterator();
    Iterator<Integer> positives = orderedNum.iterator();
    List<Integer> pairs = new ArrayList<Integer>();

    if(negatives.hasNext() && positives.hasNext()) {
      Integer negative = negatives.next();
      Integer positive = positives.next();

      while (negative >= 0 || positive <= 0) {
        if (-negative == positive) {
          pairs.add(negative);
          pairs.add(positive);
          if(negatives.hasNext() && positives.hasNext()) {
            negative = negatives.next();
            positive = positives.next();
          } else {
            break;
          }
        } else if (-negative < positive && negatives.hasNext()) {
          negative = negatives.next();
        } else if (-negative > positive && positives.hasNext()) {
          positive = positives.next();
        } else {
          break;
        }
      }
    }

    return pairs;
  }

  static void printPairs(List<Integer> pairs) {
    String delimiter = "";
    if (pairs.isEmpty()) {
      System.out.println("0");
    } else {
      for (int i = pairs.size() - 1; i >= 0 ; i--) {
        System.out.print(delimiter + pairs.get(i));
        delimiter = " ";
      }
      System.out.println();
    }
  }

	public static void main (String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int nrTest = keyboard.nextInt();

    for (int i = 0; i < nrTest; i++) {
      int length = keyboard.nextInt();
      TreeSet<Integer> treeSet = new TreeSet<>();
      for (int j = 0; j < length; j++) {
        treeSet.add(keyboard.nextInt());
      }
      printPairs(createPairs(treeSet));
    }
	}
}
