import java.util.*;
import java.lang.*;
import java.io.*;

class Graph {
  private int vertices;
  private int[][] nodes;

  public Graph(int vertices) {
    this.vertices = vertices;
    nodes = new int[vertices][vertices];
  }

  public void addEdge(int a, int b, int weight) {
    nodes[a][b] = weight;
    nodes[b][a] = weight;
  }

  public void prim() {
    boolean[] visited = new boolean[vertices];
    int[] minEdges = new int[vertices];
    int[] parents = new int[vertices];
    parents[0] = -1;

    for (int i = 1; i < vertices; i++) {
      minEdges[i] = Integer.MAX_VALUE;
    }

    for (int i = 0; i <  vertices - 1; i++) {
        int minVertex = getMinEdge(minEdges, visited);
        visited[minVertex] = true;
        for (int j = 0; j < vertices; j++) {
          if (minEdges[j] > nodes[minVertex][j] && nodes[minVertex][j] != 0 && !visited[j]) {
            minEdges[j] = nodes[minVertex][j];
            parents[j] = minVertex;
          }
        }
    }
    printMST(parents);
  }

  private int getMinEdge(int[] minEdges, boolean[] visited) {
    int min = Integer.MAX_VALUE;
    int minIndex = -1;
    for (int i = 0; i < minEdges.length; i++) {
      if (min > minEdges[i] && !visited[i]) {
        min = minEdges[i];
        minIndex = i;
      }
    }
    return minIndex;
  }

  private void printMST(int[] parents) {
    for (int i = 1; i < vertices; i++) {
      System.out.println(i + " - " + parents[i] + " -> "
      + nodes[i][parents[i]]);
    }
  }

  public static void main(String[] args) {
    Scanner keyboard = new Scanner(System.in);
    int vertices = keyboard.nextInt();
    int edges = keyboard.nextInt();
    Graph graph = new Graph(vertices);

    for (int i = 0; i < edges; i++) {
      int a = keyboard.nextInt();
      int b = keyboard.nextInt();
      int weight = keyboard.nextInt();
      graph.addEdge(a, b, weight);
    }

    graph.prim();
  }
}
